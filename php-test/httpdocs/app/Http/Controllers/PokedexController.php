<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Cache;

class PokedexController extends Controller
{
    /**
     * Create a new front instance.
     *
     * @return void
     */
    public function __construct()
    {
       //
    }

    /**
     * Show the Pokedéx listing
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        // Check if we already have Pokemon Cache data, else make Pokedex API call
        if (Cache::has('pokemon_items')) {
            $pokemon_items = collect(Cache::get('pokemon_items'));
        } else {
            // Make API call to get all Pokemon
            $guzzle = new \GuzzleHttp\Client();
            $response = $guzzle->request('GET', "http://pokeapi.co/api/v2/pokemon/", ['query' => [
                'limit' => '-1'
            ]]);
            $content = json_decode($response->getBody(), true);

            // Sort the Pokemon alphabetically
            asort($content['results']);
            $pokemon_items = [];
            // Loop through all Pokemon and build a simple array
            foreach ($content['results'] as $key => $result) {
                $pokemon_items[$key] = [
                    'name' => $result['name']
                ];
            }
            $pokemon_items = collect($pokemon_items);

            // Store all Pokemon to Cache to avoid excessive API calls
            Cache::put('pokemon_items', $pokemon_items);
        }

        return view('index', compact('pokemon_items'));
    }

    /**
     * Show the Pokemon details
     *
     * @return \Illuminate\Http\Response
     */
    public function details($name) {

        if (Cache::has($name)) {
            $details = (Cache::get($name));
        } else {
            // Get the full Pokemon details
            $pokeapi = new \PokeAPI\Client();
            $content = $pokeapi->pokemon($name);
            $details = [
                'name' => $content->getName(),
                'species' => $content->getSpecies()->getName(),
                'height' => $content->getHeight(),
                'weight' => $content->getWeight(),
                'sprites' => $content->getSprites()
            ];

            foreach ($content->getAbilities() as $ability) {
                $details['abilities'][] = $ability->getAbility()->getName();
            }

            // Store Pokemon details to Cache to avoid excessive API calls
            Cache::put($name, $details);
        }

        return view('details', compact('details'));
    }
}
