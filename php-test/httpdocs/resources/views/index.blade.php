@extends('layouts.front')

@section('content')
    <!-- Page Content -->
    <div class="container">
        <div class="card border-0 shadow my-5">
            <div class="card-body p-5">
                <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                    <h1 class="flex-sm-fill h3 my-2">Pokemon</h1>             
                </div>
                <div class="content">
                    <div class="block">
                        <div class="block-content">
                            <div class="table-responsive">
                                @if($pokemon_items->isNotEmpty())
                                    <table id="pokemon" class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($pokemon_items as $pokemon)
                                                <tr>
                                                    <td class="font-w600 font-size-sm">
                                                        {{ ucfirst($pokemon['name']) }}
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="btn-group">
                                                            <a href="{{ route('details', ['name' => $pokemon['name']]) }}" role="button" class="btn btn-sm btn-primary" data-toggle="tooltip" title="View Details">
                                                               View Details
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <p class="font-size-sm text-muted">No Pokemon available.</p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- END Full Table -->

                </div>
            </div>
        </div>
    </div>
  </div>
</div>
    <!-- END Page Content -->
@endsection

@section('css_after')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="/js/plugins/datatables/dataTables.bootstrap4.css">
    <link rel="stylesheet" href="/js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="/js/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/js/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="/js/plugins/datatables/buttons/dataTables.buttons.min.js"></script>
    <script src="/js/plugins/datatables/buttons/buttons.print.min.js"></script>
    <script src="/js/plugins/datatables/buttons/buttons.html5.min.js"></script>
    <script src="/js/plugins/datatables/buttons/buttons.flash.min.js"></script>
    <script src="/js/plugins/datatables/buttons/buttons.colVis.min.js"></script>
    <script>
        // Initiate the datatables
        $(document).ready(function() {
            $('#pokemon').DataTable();
        } );
    </script>
@endsection