@extends('layouts.front')

@section('content')
    <!-- Page Content -->
    <div class="container">
        <div class="card border-0 shadow my-5">
            <div class="card-body p-5">
                <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                    <h1 class="flex-sm-fill h3 my-2">{{ ucfirst($details['name']) }}</h1>
                </div>
                <div class="content">
                    <div class="block">
                        <div class="block-content">

                            @if($details['sprites']['front_default'])
                                <img src="{{ $details['sprites']['front_default'] }}" alt="Front default">
                            @endif
                            @if($details['sprites']['back_default'])
                                <img src="{{ $details['sprites']['back_default'] }}" alt="Back default">
                                @endif
                            @if($details['sprites']['front_shiny'])
                                <img src="{{ $details['sprites']['front_shiny'] }}" alt="Front shiny">
                                @endif
                            @if($details['sprites']['back_shiny'])
                                <img src="{{ $details['sprites']['back_shiny'] }}" alt="Back shiny">
                            @endif

                            @if($details['sprites']['front_female'])
                                <img src="{{ $details['sprites']['front_female'] }}" alt="Front Female">
                            @endif
                            @if($details['sprites']['back_female'])
                                <img src="{{ $details['sprites']['back_female'] }}" alt="Back female">
                                @endif
                            @if($details['sprites']['front_shiny_female'])
                                <img src="{{ $details['sprites']['front_shiny_female'] }}" alt="Front shiny female">
                                @endif
                            @if($details['sprites']['back_shiny_female'])
                                <img src="{{ $details['sprites']['back_shiny_female'] }}" alt="Back shiny female">
                            @endif

                            <p>Species: {{ ucfirst($details['species']) }}</p>
                            <p>Height: {{ $details['height'] }}</p>
                            <p>Weight: {{ $details['weight'] }}</p>
                            <p>Abilities:
                                @foreach ($details['abilities'] as $ability)
                                    {{ ucfirst($ability) }}@if(!$loop->last), @endif
                                @endforeach
                            </p>
                        </div>
                    </div>
                    <!-- END Full Table -->

                </div>
            </div>
        </div>
    </div>
  </div>
</div>
    <!-- END Page Content -->
@endsection