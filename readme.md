# Sizzle Challenge

## Introduction
For this challenge we would like you to expand upon your Pokèdex application.

We have downloaded the files from your php-test repository and placed them within this directory.

Any assets such as images/designs can be found in ```/assets```.

## Tech Stack
Feel free to continue to use Laravel and any other packages and libraries.

## Requirements
The requirements for this challenge have been broken down into Frontend, Database and Backend.

We advise that you spend no more than 2-3 hours on this challenge, please just complete as much as you can within this time.

### Frontend

1. Add styles to the navigation bar that match the design found here: ```/assets/navigation.png```.

2. Add the Pokédex logo to the navigation bar: ```/assets/pokedex-logo.png```.

3. Change the style of the buttons to the design found here: ```/assets/button.png``` (don't worry about using the same font).

4. Change the blue colours of the pagination to the Pokédex red.

5. Add a carousel to the Pokemon images on the 'View Details' page (you can use a JS library for this).

Please find the relevant hex colours listed below:
- Pokédex red: #EF5350
- Pokédex blue: #3634A0
- Pokédex yellow: #FFCE31

### Database

1. Create a table/migration that is suitable to store a user's favourite Pokémon(s). The table stucture will need a relational link to a user's table and the user should be able to store multiple favourite Pokémon.

### Backend

1. Integrate [Laravel's Auth](https://laravel.com/docs/5.8/authentication) and set up the default sign up/sign in functionality.

2. Implement functionality to add favourite Pokémon to the database table you previously created.

3. Implement functionality to remove Pokémon from the database table you previously created.

### Frontend and Backend

1. Add the signup/login pages from [Laravel's Auth](https://laravel.com/docs/5.8/authentication).

2. Create a view that will list the user's favourite pokemons. **This page should be accessible by logged in users only**.

3. Create a button on the above view that will allow the user to remove Pokémon from their favourites.

4. Create a view that allows the user to add Pokémons to their favourites **This page should be accessible by logged in users only**.

5. Add a link to the navigation bar that will allow the user to access their favourite Pokemon.

6. Add a link to the navigation bar that will allow the user to logout (Use the Laravel Auth for the backend functionality).

## Submission
Submit your challenge by forking this repository and sending us a link to your solution.

## Copyright
All trademarks as the property of their respective owners.
